import React, { Component } from 'react'
import buttonStyles from '../styles/Buttons.module.scss'

class LoginForm extends Component {
  getButtonClass () {
    return this.props.className ? this.props.className : buttonStyles.buttonA
  }

  getButtonType () {
    return this.props.type ? this.props.type : 'button'
  }
  render () {
    return (
      <button type={this.getButtonType()} style={this.props.style} className={this.getButtonClass()}>
        {this.props.children}
      </button>
    )
  }
}

export default LoginForm
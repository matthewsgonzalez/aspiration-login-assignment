import React, { Component } from 'react'
import inputStyles from '../styles/Inputs.module.scss'

class Input extends Component {
  constructor (props) {
    super(props)
    this.state = {
      focused: false
    }
  }
  onFocus () {
    this.setState({ focused: true })
  }


  onBlur () {
    this.setState({ focused: false })
  }
  render () {
    let labelToggleStyles = {
      opacity: this.state.focused ? '1' : '0',
      transform: this.state.focused ? 'translate(0px, 0px)' : 'translate(0px, -2px)'
    }
    return (
      <div className={inputStyles['input-wrapper']}>

        <div style={labelToggleStyles} className={inputStyles.label}>{this.props.placeholder}</div>
        <input 
        onFocus={this.onFocus.bind(this)} 
        onBlur={this.onBlur.bind(this)}
        onChange={this.props.onChange}
        name={this.props.name}
        type={this.props.type}
        placeholder={this.props.placeholder} 
        className={inputStyles.input}
      />
        <div className={inputStyles.details}>{this.props.details}</div>
      </div>
    )
  }
}

export default Input

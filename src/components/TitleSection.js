import React, { Component } from 'react'
import titleStyles from '../styles/Titles.module.scss'
class TitleSection extends Component {
  render () {
    return (
      <div className={titleStyles['title-wrapper']}>
          {this.props.children}
      </div>
    )
  }
}

export default TitleSection
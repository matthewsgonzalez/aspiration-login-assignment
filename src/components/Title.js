import React, { Component } from 'react'
import titleStyles from '../styles/Titles.module.scss'
class Title extends Component {
  render () {
    return (
      <div className={titleStyles.title}>
          {this.props.children}
      </div>
    )
  }
}

export default Title

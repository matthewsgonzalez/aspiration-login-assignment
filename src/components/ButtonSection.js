import React, { Component } from 'react'
import buttonStyles from '../styles/Buttons.module.scss'

class ButtonSection extends Component {
  render () {
    return (
      <div className={buttonStyles.buttonSection}>
        {this.props.children}
      </div>
    )
  }
}

export default ButtonSection
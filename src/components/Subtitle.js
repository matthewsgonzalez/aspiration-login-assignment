import React, { Component } from 'react'
import titleStyles from '../styles/Titles.module.scss'
class Subtitle extends Component {
  render () {
    return (
      <div className={titleStyles.subtitle}>
          {this.props.children}
      </div>
    )
  }
}

export default Subtitle

import React, { Component } from 'react';
import {
  BrowserRouter as Router
} from 'react-router-dom'
import logo from './assets/aspiration-logo.svg'

import { Authentication } from './components'
import styles from './styles/App.module.scss';

class App extends Component {
  render() {
    return (
      <Router>
        <div className={styles.App}>
          <div style={{margin:'auto'}}>
            <img className={styles.logo} src={logo}/>
            <Authentication/>
          </div>
          <span className={styles["project-info"]}>
            Aspiration Login-form Interview Assignment completed by <a href="http://matthewgonzalez.me" target="_blank" rel="noopener noreferrer">Matthew Gonzalez</a>
          </span>
        </div>
      </Router>
    );
  }
}

export default App;

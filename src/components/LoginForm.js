import React, { Component } from 'react'
import { 
  TitleSection, 
  Title, 
  Subtitle, 
  ButtonSection, 
  Button, 
  Input 
} from '.'
import styles from '../styles/App.module.scss'

class LoginForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
      user: {
        email: null,
        password: null
      }
    }
  }

  changeUser(event) {
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;

    this.setState({
      user
    });
  }

  onSubmit (e) {
    e.preventDefault();
    if (this.state.user.email && this.state.user.password) {
      alert('This is where we\'d post something to our backend with: \n\nEmail: ' + this.state.user.email + '\n' +
      'Password: [confidential]'
      )
    } else {
      alert('Please enter your login information.')
    }
    
  }

  render () {
    return (
      <form className={styles['authentication-content']} onSubmit={this.onSubmit.bind(this)} >
        <div>
          <TitleSection>
            <Title>
          Welcome Back!</Title>
            <Subtitle>
            Enter your email address and password below to log in.</Subtitle>
          </TitleSection>
            <Input 
              onChange={this.changeUser.bind(this)} 
              details="Your email address must contain an @ symbol." 
              placeholder="Email address" 
              name="email"
            />
            <Input
              onChange={this.changeUser.bind(this)} 
              type="password" 
              placeholder="Password"
              name="password" 
            />
        </div>
        <ButtonSection>
          <Button type="submit">Log in</Button>
        </ButtonSection>
      </form>
    )
  }
}

export default LoginForm
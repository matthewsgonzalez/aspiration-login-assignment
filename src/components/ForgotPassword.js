import React, { Component } from 'react'
import {TitleSection, Title, Subtitle, ButtonSection, Button, Input } from '.'
import titleStyles from '../styles/Titles.module.scss'
import buttonStyles from '../styles/Buttons.module.scss'
import styles from '../styles/App.module.scss'

class ForgotPassword extends Component {
  constructor (props) {
    super(props)

    this.state = {
      user: {
        email: null
      }
    }
  }

  changeUser(event) {
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;

    this.setState({
      user
    });
  }

  onSubmit (e) {
    e.preventDefault();
    if (this.state.user.email) {
      alert('This is where we\'d post something to our backend with: \n\nEmail: ' + this.state.user.email
      )
    } else {
      alert('Please enter your email.')
    }
    
  }

  render () {
    return (
      <form onSubmit={this.onSubmit.bind(this)} className={styles['authentication-content']}> 
        <div>
          <TitleSection>
            <Title>
          Forgot your password?</Title>
            <Subtitle>
          Enter your email address below to recieve a password reset email.</Subtitle>
          </TitleSection>
          <Input 
            details="Your email address must contain an @ symbol." 
            placeholder="Email address" 
            onChange={this.changeUser.bind(this)}
            name="email"
          />
        </div>
        <ButtonSection>
          <Button type="submit" style={{width: '100%'}} className={buttonStyles.buttonB}>Send Password Reset Email</Button>
        </ButtonSection>
      </form>
    )
  }
}

export default ForgotPassword

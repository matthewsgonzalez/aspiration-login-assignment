import React, { Component } from 'react'
import {
  BrowserRouter as Router,
  Route,
  NavLink
} from 'react-router-dom'

import {LoginForm, ForgotPassword} from '.'
import styles from '../styles/App.module.scss'



class Authentication extends Component {
  constructor (props) {
    super(props)
  }
  render () {
    let navStyles = {
      display: 'flex',
      width: '100%',
      textAlign: 'center'
    }
    let navItemStyles = function(showDivider=true) {
      return {
        flexGrow: '1',
        flexBasis: '150px',
        borderRight: showDivider ? '1px solid #ddd' : '0px solid transparent',
        padding: '15px 5px 12px',
        fontWeight: 'bold',
        textDecoration: 'none',
        color: '#2E3545',
        fontSize: '16px',
        borderBottom: '1px solid #ddd'
      }
    }

    let navItemActiveStyles = {
      color: '#1165BF',
      borderBottom: '4px solid #1165BF'
    }

    let routeContainerStyles = {
      padding: '15px'
    }
    return (
      <div className={styles.authentication}>
        <div style={navStyles}>
          <NavLink 
            exact
            style={navItemStyles()} 
            activeStyle={navItemActiveStyles} 
            to="/">
            Log In
          </NavLink>
          <NavLink 
            exact
            style={navItemStyles(false)} 
            activeStyle={navItemActiveStyles} 
            to="/forgot-password">
            Forgot Password
          </NavLink>
        </div>
        <Route 
          exact 
          path="/" 
          component={LoginForm}
        />
        <Route 
          exact 
          path="/forgot-password" 
          component={ForgotPassword}
        />
      </div>
    )
  }
}

export default Authentication